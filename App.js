import React from 'react';
import {View, StyleSheet} from 'react-native';

const App = () => {
  return (
    <View style={styles.container}>
      <View style={styles.first} />
      <View style={styles.second} />
      <View style={styles.last} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  first: {
    flex: 1,
    backgroundColor: 'powderblue',
  },
  second: {
    flex: 2,
    backgroundColor: 'skyblue',
  },
  last: {
    flex: 3,
    backgroundColor: 'steelblue',
  },
});

export default App;
